print("Bine ati venit!")
cuv = input("Introduceti un cuvant: ")
print("Introduceti o litera: ")
corecte = ''
incercari = 7
while incercari > 0:
    gresite = 0
    for i in cuv:
        if i == cuv[0]:
            print(i)
        elif i == cuv[-1]:
            print(i)
        elif i in corecte:
            print(i)
        else:
            print("_")
            gresite += 1
    if gresite == 0:
        print("Ati castigat!")
        print("Cuvantul este: {}".format(cuv))
        break
    lit = input("Introduceti o litera: ")
    if lit in corecte:
        print("Litera a mai fost introdusa, incercati alta: ")
    corecte += lit
    if lit not in cuv:
        incercari -= 1
        print("Litera introdusa este gresita!")
        print("Mai aveti {} incercari.".format(incercari))
        if incercari == 0:
            print("Ati pierdut!")
